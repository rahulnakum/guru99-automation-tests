package Pages;

import org.openqa.selenium.By;

public class BasePage {
    public By linkAddCustomer = By.linkText("Add Customer");
    public By btnSubmit = By.xpath("//input[@type='submit']");
    public By btnRest = By.xpath("//input[@type='reset']");
    public By linkGuru99Telecom =  By.linkText("Guru99 telecom");

}
