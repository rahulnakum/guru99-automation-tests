package Utilities;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.internal.TestResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class BaseTest {

    public WebDriver driver;
    public Properties properties;

    @BeforeClass
    public void classSetup() throws IOException {
        //read properties
        properties = new Properties();
        properties.load(new FileInputStream((System.getProperty("user.dir") + "\\src\\test\\resources\\project.properties")));

        //setup cromedriver
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") +properties.getProperty("chromeDriverPath"));
        //set Firefox driver
        System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") +properties.getProperty("gecoDriverPath"));
        //IE
        System.setProperty("webdriver.ie.driver",System.getProperty("user.dir") +properties.getProperty("IEDriverPath"));

        String browserName = properties.getProperty("browser");

        if(browserName.toUpperCase().equals("CHROME")){
            driver = new ChromeDriver();
        }
        else if(browserName.toUpperCase().equals("FIREFOX")){
            driver = new FirefoxDriver();
        }
        else if(browserName.toUpperCase().equals("IE")){
            driver = new InternetExplorerDriver();
        }

        driver.manage().window().maximize();
        driver.get(properties.getProperty("baseUrl"));
    }

    //Take screenshots only for failed tests
    @AfterMethod
    public void afterEachTest(ITestResult result, Method method) throws IOException {
        if(!result.isSuccess())
        {
            String testName = method.getName();
            String dateInString =new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            String dateTime = new Date().toString();
            //take screenshot
            TakesScreenshot obj1 = (TakesScreenshot) driver;
            File file1 = obj1.getScreenshotAs(OutputType.FILE);
            File file2 = new File(System.getProperty("user.dir")+"\\src\\test\\resouces\\ScreenShots\\"+testName+"_"+dateInString+".png");
            FileUtils.copyFile(file1, file2);
        }
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
